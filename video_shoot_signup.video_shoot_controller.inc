<?php

/**
 * @file
 * Interface and Controller for Marketing Video Shoots.
 */

/**
 * VideoShoot class.
 */
class VideoShoot extends Entity {
  protected function defaultLabel() {
    return t('@title', array('@title' => $this->title));
  }
  protected function defaultUri() {
    return array('path' => 'admin/config/hfc/video-shoot/' . $this->identifier());
  }
}

/**
 * VideoShootController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class VideoShootController extends EntityAPIController {

  /**
   * Define display properties for entity contents.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $shoot_types = video_shoot_signup_types();

    $content['start_date'] = array(
      '#field_name' => 'video_signup_shoot_start_date',
      '#weight' => -9,
      '#label' => t('Start Time'),
      '#label_display' => 'inline',
      '#theme' => 'hfcc_global_pseudo_field',
      '#markup' => format_date($entity->start_date, 'medium'),
    );
    $content['end_date'] = array(
      '#field_name' => 'video_signup_shoot_end_date',
      '#weight' => -8,
      '#label' => t('End Time'),
      '#label_display' => 'inline',
      '#theme' => 'hfcc_global_pseudo_field',
      '#markup' => format_date($entity->end_date, 'medium'),
    );
    if (!empty($entity->type)) {
      $content['shoot_type'] = array(
        '#field_name' => 'video_signup_shoot_shoot_type',
        '#weight' => -6,
        '#label' => t('Shoot Type'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => !empty($shoot_types[$entity->type]) ? t($shoot_types[$entity->type]) : check_plain($entity->type),
      );
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
