<?php

/**
 * Includes page callbacks for the video shoot signup module.
 */

/**
 * Page callback for scheduler.
 */
function video_shoot_signup_schedule_page() {
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('For HFC Employees'), 'faculty-and-staff'),
    l(t('HFC Program Marketing Video Shoot'), 'faculty-and-staff/video-shoot'),
  ));

  return video_shoot_signup_schedule();
}

/**
 * Scheduler display.
 */
function video_shoot_signup_schedule() {
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('For HFC Employees'), 'faculty-and-staff'),
    l(t('HFC Program Marketing Video Shoot'), 'faculty-and-staff/video-shoot'),
  ));

  $output = array();

  if ($result = db_query("SELECT item.* FROM {video_signup_schedule} item LEFT JOIN {video_signup_shoots} shoot ON item.shoot_id = shoot.shoot_id ORDER BY shoot.type, item.start_date")->fetchAll()) {
    $rows = array();
    foreach ($result as $item) {
      $actions = array(l(t('view'), 'faculty-and-staff/video-shoot/schedule/' . $item->schedule_id));
      if (entity_access('update', 'video_signup_schedule', $item)) {
        $actions[] = l(
          t('edit'),
          'faculty-and-staff/video-shoot/schedule/' . $item->schedule_id . '/edit',
          array('query' => drupal_get_destination())
        );
      }

      $rows[$item->shoot_id][] = array(
        'day' => format_date($item->start_date, 'custom', 'l, F j'),
        'time' => format_date($item->start_date, 'custom', 'g:ia') . ' - ' . format_date($item->end_date, 'custom', 'g:ia'),
        'program_name' => check_plain($item->program_name),
        'contact_name' => check_plain($item->contact_name),
        'location' => check_plain($item->location),
        'actions' => $actions,
        '#locked' => $item->locked,
        '#item' => $item,
      );
    }
    $shoots = entity_load('video_signup_shoot', array_keys($rows));
    foreach ($rows as $key => $schedule) {
      $output[] = array(
        '#prefix' => '<h3>',
        '#markup' => check_plain($shoots[$key]->title),
        '#suffix' => '</h3>',
      );
      $last_day = NULL;
      $display = array();
      foreach ($schedule as $item) {
        // Set a date header row if the day has changed.
        if ($item['day'] !== $last_day) {
          $display[] = array(array('data' => $item['day'], 'colspan' => '5', 'class' => array('video-shoot-day-heading')));
          $last_day = $item['day'];
        }
        if ($item['#item']->locked == -1) {
          // Indicate breaks.
          $display[] = array(
            'data' => array(
              $item['time'],
              array('data' => $item['program_name'], 'colspan' => '4', 'class' => array('video-shoot-break')),
            ),
            'class' => array('video-schedule-' . $item['#item']->schedule_id),
          );
        }
        else {
          // Display the row normally.
          $display[] = array(
            'data' => array(
              $item['time'],
              $item['program_name'],
              !empty($item['contact_name']) ? video_shoot_signup_get_contact($item['contact_name']) : NULL,
              $item['location'],
              implode(' ', $item['actions']),
            ),
            'class' => array('video-schedule-' . $item['#item']->schedule_id),
          );
        }
      }

      $output[] = array(
        '#theme' => 'table',
        '#header' => array(t('Time'), t('Program'), t('Faculty Contact'), t('Class/Lab Location for Shoot'), t('Action')),
        '#rows' => $display,
      );
    }
  }
  return $output;
}

/**
 * Display a schedule item.
 */
function video_shoot_signup_schedule_item($entity) {
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('For HFC Employees'), 'faculty-and-staff'),
    l(t('Video Shoot Signup'), 'faculty-and-staff/video-shoot'),
  ));
  drupal_set_title($entity->label());
  return $entity->view();
}

/**
 * Create a new schedule item.
 */
function video_shoot_signup_schedule_add() {
  $entity = entity_create('video_signup_schedule', array(
    'start_date' => time(),
    'end_date' => time(),
    'locked' => 0,
  ));
  // entity_save('video_signup_schedule', $entity);
  return drupal_get_form('video_shoot_signup_schedule_form', $entity);
}

/**
 * Edit a schedule item.
 */
function video_shoot_signup_schedule_form($form, &$form_state, $entity) {
  $uri = $entity->uri();
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('For HFC Employees'), 'faculty-and-staff'),
    l(t('Video Shoot Signup'), 'faculty-and-staff/video-shoot'),
    l(check_plain($entity->label()), $uri['path']),
  ));
  drupal_set_title(t('<em>Editing signup for</em> @label', array('@label' => $entity->label())), PASS_THROUGH);
  $form = array();

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['shoot_id'] = array(
    '#type' => 'select',
    '#title' => t('Shoot'),
    '#options' => video_shoot_signup_shoots_opts(),
    '#default_value' => !empty($entity->shoot_id) ? $entity->shoot_id : NULL,
    '#disabled' => !empty($entity->schedule_id),
    '#required' => TRUE,
    '#weight' => -9,
  );
  $form['start_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Start Time'),
    '#date_format' => 'm/d/Y h:ia',
    '#default_value' => format_date($entity->start_date, 'custom', 'Y-m-d H:i:s'),
    '#disabled' => !user_access('administer video shoot signup'),
    '#required' => TRUE,
    '#weight' => -8,
  );
  $form['end_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('End Time'),
    '#date_format' => 'm/d/Y h:ia',
    '#default_value' => format_date($entity->end_date, 'custom', 'Y-m-d H:i:s'),
    '#disabled' => !user_access('administer video shoot signup'),
    '#required' => TRUE,
    '#weight' => -7,
  );
  $form['program_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Program'),
    '#default_value' => trim($entity->program_name),
    '#description' => t('Participation is limited to <a href="!url">these listed programs</a>.', array("!url" => '/faculty-and-staff/video-shoot/programs')),
    '#size' => 30,
    '#maxlength' => 100,
    '#required' => !user_access('administer video shoot signup'),
  );
  $form['contact_name'] = array(
    '#type' => 'select',
    '#options' => array('' => '-- select --') + hfccwsclient_get_staffdir_hankid_opts(),
    '#title' => t('Faculty Contact'),
    '#default_value' => trim($entity->contact_name),
    '#description' => t('Please specify the faculty contact for this session.'),
    '#required' => !user_access('administer video shoot signup'),
  );
  $form['location'] = array(
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#default_value' => trim($entity->location),
    '#description' => t('Please specify the classroom/lab location for the video shoot. (Note: Locations for interviews will be announced by Marketing and Communications.)'),
    '#size' => 30,
    '#maxlength' => 100,
    '#required' => FALSE,
  );
  $form['notes'] = array(
    '#type' => 'textarea',
    '#title' => t('Notes'),
    '#default_value' => trim($entity->notes),
    '#required' => FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  return $form;
}

/**
 * Validation handler for form.
 */
function video_shoot_signup_schedule_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (!empty($values['shoot_id'])) {
    $shoot = entity_load('video_signup_shoot', array($values['shoot_id']));
    $shoot = reset($shoot);
    $shoot->end_date += 86400;

    $start_date = strtotime($form_state['values']['start_date']);
    $end_date = strtotime($form_state['values']['end_date']);

    if ($start_date < $shoot->start_date || $start_date > $shoot->end_date) {
      form_set_error('start_date', 'Start date does not fall between shoot start and end dates.');
    }
    if ($end_date < $shoot->start_date || $end_date > $shoot->end_date) {
      form_set_error('end_date', 'End date does not fall between shoot start and end dates.');
    }
  }
}

/**
 * Submit handler for form.
 */
function video_shoot_signup_schedule_form_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];

  if (empty($entity->shoot_id)) {
    $entity->shoot_id = $form_state['values']['shoot_id'];
  }
  $entity->start_date = strtotime($form_state['values']['start_date']);
  $entity->end_date = strtotime($form_state['values']['end_date']);
  $entity->program_name = $form_state['values']['program_name'];
  $entity->contact_name = $form_state['values']['contact_name'];
  $entity->location = $form_state['values']['location'];
  $entity->notes = $form_state['values']['notes'];
  entity_save('video_signup_schedule', $entity);
  $form_state['redirect'] = $entity->uri();

}

/**
 * Confirmation form to reset a scheduled item.
 */
function video_shoot_signup_schedule_reset_confirm($form, &$form_state, $entity) {
  $uri = $entity->uri();
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('For HFC Employees'), 'faculty-and-staff'),
    l(t('Video Shoot Signup'), 'faculty-and-staff/video-shoot'),
    l(check_plain($entity->label()), $uri['path']),
  ));

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  // Always provide entity id in the same form key as in the entity edit form.
  $form['schedule_id'] = array(
    '#type' => 'value',
    '#value' => $entity->schedule_id,
  );

  return confirm_form($form,
    t('Are you sure you want to reset this entry for %program_name?', array('%program_name' => $entity->label())),
    $entity->uri(), t('This action cannot be undone.'), t('Reset'), t('Cancel')
  );
}

/**
 * Submit handler for reset confirmation.
 */
function video_shoot_signup_schedule_reset_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] == 1 && !empty($form_state['values']['schedule_id'])) {
    $entity = $form_state['values']['entity'];
    $entity->program_name = NULL;
    $entity->contact_name = NULL;
    $entity->location = NULL;
    $entity->notes = NULL;
    entity_save('video_signup_schedule', $entity);
    $form_state['redirect'] = $entity->uri();
  }
}

/**
 * Confirmation form to delete a scheduled item.
 */
function video_shoot_signup_schedule_delete_confirm($form, &$form_state, $entity) {
  $uri = $entity->uri();
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('For HFC Employees'), 'faculty-and-staff'),
    l(t('Video Shoot Signup'), 'faculty-and-staff/video-shoot'),
    l(check_plain($entity->label()), $uri['path']),
  ));

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  // Always provide entity id in the same form key as in the entity edit form.
  $form['schedule_id'] = array(
    '#type' => 'value',
    '#value' => $entity->schedule_id,
  );

  return confirm_form($form,
    t('Are you sure you want to <em>delete</em> this entry for %program_name?', array('%program_name' => $entity->label())),
    $entity->uri(), t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Submit handler for delete confirmation.
 */
function video_shoot_signup_schedule_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] == 1 && !empty($form_state['values']['schedule_id'])) {
    $entity = $form_state['values']['entity'];
    $num_deleted = db_delete('video_signup_schedule')
      ->condition('schedule_id', $form_state['values']['schedule_id'])
      ->execute();
    $values = array('%schedule_id' => $form_state['values']['schedule_id']);
    if ($num_deleted == 1) {
      drupal_set_message(t('Item %schedule_id deleted.', $values));
    }
    else {
      watchdog('video_shoot_signup', 'Something bad happened deleting item %schedule_id', $values, WATCHDOG_ERROR);
    }
  }
  drupal_goto('faculty-and-staff/video-shoot');
}
