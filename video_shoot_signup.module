<?php

/**
 * @file
 * Creates a sign-up form for HFC marketing video shoots.
 */

/**
 * Implements hook_permission().
 */
function video_shoot_signup_permission() {
  return array(
    'administer video shoot signup' => array(
      'title' => t('Administer video shoot signup'),
      'description' => t('Administer video shoot items.'),
    ),
    'view video shoot signup' => array(
      'title' => t('View video shoot signup'),
      'description' => t('View video shoot signup forms.'),
    ),
    'edit video shoot signup' => array(
      'title' => t('Edit video shoot signup'),
      'description' => t('Sign up for a timeslot.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function video_shoot_signup_menu() {
  $items['admin/config/hfc/video-shoot'] = array(
    'title' => 'Video Shoot Schedule',
    'description' => 'Administer video shoot signup form',
    'page callback' => 'video_shoot_signup_admin_main',
    'access arguments' => array('administer video shoot signup'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'video_shoot_signup.admin.inc',
  );
  $items['admin/config/hfc/video-shoot/%video_shoot_signup_shoot'] = array(
    'title' => 'Video shoot details',
    'description' => 'View shoot details',
    'page callback' => 'video_shoot_signup_shoot_item',
    'page arguments' => array(4),
    'access arguments' => array('administer video shoot signup'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'video_shoot_signup.admin.inc',
  );
  $items['admin/config/hfc/video-shoot/%video_shoot_signup_shoot/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => '-10',
  );
  $items['admin/config/hfc/video-shoot/%video_shoot_signup_shoot/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit shoot details',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('video_shoot_signup_shoot_form', 4),
    'access arguments' => array('administer video shoot signup'),
    'file' => 'video_shoot_signup.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/hfc/video-shoot/%video_shoot_signup_shoot/delete'] = array(
    'title' => 'Delete',
    'description' => 'Delete video shoot',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('video_shoot_signup_shoot_delete_confirm', 4),
    'access arguments' => array('administer video shoot signup'),
    'file' => 'video_shoot_signup.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 50,
  );
  $items['admin/config/hfc/video-shoot/add'] = array(
    'title' => 'Add video shoot',
    'description' => 'Add a new video shoot',
    'page callback' => 'video_shoot_signup_shoot_add',
    'access arguments' => array('administer video shoot signup'),
    'file' => 'video_shoot_signup.admin.inc',
    'type' => MENU_LOCAL_ACTION,
  );
  $items['admin/config/hfc/video-shoot/truncate'] = array(
    'title' => 'Truncate schedule',
    'description' => 'Clear the schedule table',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('video_shoot_signup_schedule_truncate_confirm'),
    'access arguments' => array('administer video shoot signup'),
    'file' => 'video_shoot_signup.admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['faculty-and-staff/video-shoot/schedule'] = array(
    'title' => 'Video Shoot Schedule',
    'description' => 'Video shoot signup form',
    'page callback' => 'video_shoot_signup_schedule_page',
    'access arguments' => array('view video shoot signup'),
    'file' => 'video_shoot_signup.pages.inc',
  );
  $items['faculty-and-staff/video-shoot/schedule/%video_shoot_signup_schedule'] = array(
    'title' => 'View schedule item',
    'description' => 'View scheduled item',
    'page callback' => 'video_shoot_signup_schedule_item',
    'page arguments' => array(3),
    'access callback' => 'entity_access',
    'access arguments' => array('view', 'video_signup_schedule', 3),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'video_shoot_signup.pages.inc',
  );
  $items['faculty-and-staff/video-shoot/schedule/%video_shoot_signup_schedule/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => '-10',
  );
  $items['faculty-and-staff/video-shoot/schedule/%video_shoot_signup_schedule/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit scheduled item',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('video_shoot_signup_schedule_form', 3),
    'access callback' => 'entity_access',
    'access arguments' => array('update', 'video_signup_schedule', 3),
    'file' => 'video_shoot_signup.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['faculty-and-staff/video-shoot/schedule/%video_shoot_signup_schedule/reset'] = array(
    'title' => 'Reset',
    'description' => 'Reset scheduled item',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('video_shoot_signup_schedule_reset_confirm', 3),
    'access arguments' => array('administer video shoot signup'),
    'file' => 'video_shoot_signup.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['faculty-and-staff/video-shoot/schedule/%video_shoot_signup_schedule/delete'] = array(
    'title' => 'Delete',
    'description' => 'Delete scheduled time',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('video_shoot_signup_schedule_delete_confirm', 3),
    'access arguments' => array('administer video shoot signup'),
    'file' => 'video_shoot_signup.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 50,
  );
  $items['faculty-and-staff/video-shoot/schedule/add'] = array(
    'title' => 'Add time slot',
    'description' => 'Add a new scheduled time',
    'page callback' => 'video_shoot_signup_schedule_add',
    'access arguments' => array('administer video shoot signup'),
    'file' => 'video_shoot_signup.pages.inc',
    'type' => MENU_LOCAL_ACTION,
  );
  return $items;
}

/**
 * Implements hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function video_shoot_signup_block_info() {
  $blocks = array();
  $blocks['video_shoot_schedule'] = array(
    'info' => t('Video Shoot Schedule.'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function video_shoot_signup_block_view($delta = '') {
  module_load_include('inc', 'video_shoot_signup', 'video_shoot_signup.pages');
  $block = array();
  switch ($delta) {
    case 'video_shoot_schedule':
      $block['subject'] = t('Video Shoot Schedule');
      $block['content'] = video_shoot_signup_schedule();
      break;
  }
  return $block;
}

/**
 * Implements hook_entity_info().
 *
 * @see http://www.istos.it/blog/drupal-entities/drupal-entities-part-3-programming-hello-drupal-entity
 * @see http://drupal.org/node/1026420
 */
function video_shoot_signup_entity_info() {
  return array(
    'video_signup_shoot' => array(
      'label' => t('Video Shoot'),
      'entity class' => 'VideoShoot',
      'controller class' => 'VideoShootController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'video_signup_shoots',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'video_shoot_signup_access',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'shoot_id',
      ),
      'static cache' => TRUE,
      'bundles' => array(
        'video_signup_shoot' => array(
          'label' => 'Video Shoot',
          'admin' => array(
            'path' => 'admin/config/hfc/video-shoot/shoots',
            'access arguments' => array('administer video shoot signup'),
          ),
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full content'),
          'custom settings' => FALSE,
        ),
      ),
    ),
    'video_signup_schedule' => array(
      'label' => t('Video Session Schedule'),
      'entity class' => 'VideoSchedule',
      'controller class' => 'VideoScheduleController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'video_signup_schedule',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'video_shoot_signup_access',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'schedule_id',
      ),
      'static cache' => TRUE,
      'bundles' => array(
        'video_signup_schedule' => array(
          'label' => 'Video Session Schedule',
          'admin' => array(
            'path' => 'admin/config/hfc/video-shoot/schedule',
            'access arguments' => array('administer video shoot signup'),
          ),
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full content'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
}

/**
 * Access callback for entities.
 *
 * Arguments for this function are defined by entity_access().
 * @see entity_access()
 */
function video_shoot_signup_access($op, $entity = NULL, $account = NULL, $entity_type = NULL) {
  if (user_access('administer video shoot signup')) {
    return TRUE;
  }
  if ($entity_type == 'video_signup_schedule') {
    switch ($op) {
      case 'view':
        return user_access('view video shoot signup');
      case 'update':
        return user_access('edit video shoot signup') && empty($entity->program_name) && !$entity->locked;
      case 'create':
      case 'delete':
        // Only admins can create or delete records.
    }
  }
}

/**
 * Load a single shoot record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function video_shoot_signup_shoot_load($id) {
  $entity = entity_load('video_signup_shoot', array($id));
  return $entity ? reset($entity) : FALSE;
}

/**
 * Load a single schedule record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function video_shoot_signup_schedule_load($id) {
  $entity = entity_load('video_signup_schedule', array($id));
  return $entity ? reset($entity) : FALSE;
}

/**
 * Get contact details from Web Services Client
 */
function video_shoot_signup_get_contact($id, $type = 'name') {
  if ($person = hfccwsclient_get_staffdir($filter_opts = array('id=' . $id))) {
    $person = reset($person);
    $last_name = !empty($person['lastname']) ? $person['lastname'] : '';
    $first_name = !empty($person['firstname']) ? $person['firstname'] : '';
    $fullname = $first_name . ' ' . $last_name;

    switch ($type) {
      case 'full':
        $output = array();
        $output[] = $fullname;
        if (!empty($person['title'])) {
          $output[] = check_plain($person['title']);
        }
        if (!empty($person['email_address'])) {
          $output[] = check_plain($person['email_address']);
        }
        if (!empty($person['phone_number'])) {
          $output[] = check_plain($person['phone_number']);
        }
        return implode('<br>', $output);
      default:
        return check_plain($fullname);
    }
  }
  else {
    drupal_set_message(t('Cannot lookup person @id', array('@id' => $id)), 'error');
    return($id);
  }
}

/**
 * Return a list of shoot types.
 */
function video_shoot_signup_types() {
  return array(
    'shoot' => 'Video Shoot',
    'interview' => 'Student Interview',
  );
}

/**
 * Get list of shoots as options.
 */
function video_shoot_signup_shoots_opts() {
  $output = array('' => '-- select --');

  if ($result = db_query("SELECT * FROM {video_signup_shoots} ORDER BY start_date,shoot_id")->fetchAll()) {
    foreach ($result as $item) {
      $output[$item->shoot_id] = decode_entities(check_plain($item->title));
    }
  }
  return $output;
}
