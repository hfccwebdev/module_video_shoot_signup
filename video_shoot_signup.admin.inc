<?php

/**
 * @file
 * Administration interface for video shoot signup.
 */

/**
 * Main administration interface.
 */
function video_shoot_signup_admin_main() {
  $output = array();

  $output[] = array(
    '#prefix' => '<h3>',
    '#markup' => t('Available Video Shoots'),
    '#suffix' => '</h3>',
  );

  if ($result = db_query("SELECT * FROM {video_signup_shoots} ORDER BY start_date,shoot_id")->fetchAll()) {
    $rows = array();
    foreach ($result as $item) {
      $actions = array(
        l('view', 'admin/config/hfc/video-shoot/' . $item->shoot_id),
        l('edit', 'admin/config/hfc/video-shoot/' . $item->shoot_id . '/edit'),
        l('delete', 'admin/config/hfc/video-shoot/' . $item->shoot_id . '/delete'),
      );
      $rows[] = array(
        check_plain($item->title),
        check_plain($item->type),
        format_date($item->start_date, 'custom', 'D, M j, Y'),
        format_date($item->end_date, 'custom', 'D, M j, Y'),
        implode(' ', $actions),
      );
    }
    $output[] = array(
      '#theme' => 'table',
      '#header' => array(t('Shoot'), t('Type'), t('Start date'), t('End date'), t('Actions')),
      '#rows' => $rows,
    );
  }

  $output[] = array(
    '#prefix' => '<h3>',
    '#markup' => t('Video Shoot Schedule'),
    '#suffix' => '</h3>',
  );
  $output[] = array(
    '#prefix' => '<p>',
    '#markup' => l(t('Manage video shoot schedule items'), 'faculty-and-staff/video-shoot/schedule'),
    '#suffix' => '</p>',
  );

  if (user_access('administer video shoot signup')) {
    $output[] = array(
      '#prefix' => '<p>',
      '#markup' => l(t('Remove all video shoot schedule items'), 'admin/config/hfc/video-shoot/truncate'),
      '#suffix' => '</p>',
    );
  }

  return $output;
}

/**
 * Page callback to display shoot details.
 */
function video_shoot_signup_shoot_item($entity) {
  drupal_set_title($entity->label());
  return $entity->view();
}

/**
 * Create a new video shoot.
 */
function video_shoot_signup_shoot_add() {
  $entity = entity_create('video_signup_shoot', array(
    'start_date' => time(),
    'end_date' => time(),
    'type' => 'shoot',
  ));
  return drupal_get_form('video_shoot_signup_shoot_form', $entity);
}

/**
 * Edit a shoot item.
 */
function video_shoot_signup_shoot_form($form, &$form_state, $entity) {
  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );
  $form['start_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Start Time'),
    '#date_format' => 'm/d/Y',
    '#default_value' => format_date($entity->start_date, 'custom', 'Y-m-d H:i:s'),
    '#required' => TRUE,
    '#weight' => -9,
  );
  $form['end_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('End Time'),
    '#date_format' => 'm/d/Y',
    '#default_value' => format_date($entity->end_date, 'custom', 'Y-m-d H:i:s'),
    '#required' => TRUE,
    '#weight' => -8,
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => video_shoot_signup_types(),
    '#default_value' => !empty($entity->type) ? $entity->type : NULL,
    '#required' => TRUE,
    '#weight' => -7,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Validation handler for form.
 */
function video_shoot_signup_shoot_form_validate($form, &$form_state) {
  if (strtotime($form_state['values']['start_date']) > strtotime($form_state['values']['end_date'])) {
    form_set_error('start_date', 'Start date must be before end date.');
  }
}

/**
 * Submit handler for form.
 */
function video_shoot_signup_shoot_form_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];

  $entity->start_date = strtotime($form_state['values']['start_date']);
  $entity->end_date = strtotime($form_state['values']['end_date']);
  $entity->type = $form_state['values']['type'];

  $title = format_date($entity->start_date, 'custom', 'F j-');
  if (format_date($entity->start_date, 'custom', 'm') !== format_date($entity->end_date, 'custom', 'm')) {
    $title .= format_date($entity->end_date, 'custom', 'F j ');
  }
  else {
    $title .= format_date($entity->end_date, 'custom', 'j ');
  }
  $types = video_shoot_signup_types();
  $title .= $types[$entity->type];
  $entity->title = $title;

  entity_save('video_signup_shoot', $entity);
  $form_state['redirect'] = 'admin/config/hfc/video-shoot';
}

/**
 * Confirmation form to delete a scheduled item.
 */
function video_shoot_signup_shoot_delete_confirm($form, &$form_state, $entity) {

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  // Always provide entity id in the same form key as in the entity edit form.
  $form['shoot_id'] = array(
    '#type' => 'value',
    '#value' => $entity->shoot_id,
  );

  return confirm_form($form,
    t('Are you sure you want to <em>delete</em> the %title shoot and all of its timeslots?', array('%title' => $entity->label())),
    $entity->uri(), t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Submit handler for delete confirmation.
 */
function video_shoot_signup_shoot_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] == 1 && !empty($form_state['values']['shoot_id'])) {
    $entity = $form_state['values']['entity'];
    $num_deleted = db_delete('video_signup_schedule')
      ->condition('shoot_id', $form_state['values']['shoot_id'])
      ->execute();
    $num_deleted = db_delete('video_signup_shoots')
      ->condition('shoot_id', $form_state['values']['shoot_id'])
      ->execute();
    $values = array('%shoot_id' => $form_state['values']['shoot_id']);
    if ($num_deleted == 1) {
      drupal_set_message(t('Item %shoot_id deleted.', $values));
    }
    else {
      watchdog('video_shoot_signup', 'Something bad happened deleting item %shoot_id', $values, WATCHDOG_ERROR);
    }
  }
  drupal_goto('admin/config/hfc/video-shoot');
}

/**
 * Confirmation form to truncate schedule table.
 */
function video_shoot_signup_schedule_truncate_confirm($form, &$form_state) {
  $form['truncate'] = array(
    '#type' => 'textfield',
    '#title' => t('Verification'),
    '#default_value' => NULL,
    '#description' => t('Type <em>truncate</em> to delete all data.'),
    '#size' => 30,
    '#required' => TRUE,
  );
  return confirm_form(
    $form,
    t('Are you sure you want to <em>truncate</em> the schedule table?'),
    'admin/config/hfc/video-shoot',
    t('This action <strong>will remove all entries</strong> and cannot be undone.'),
    t('Truncate'),
    t('Cancel')
  );
}

/**
 * Submit handler for truncate confirmation.
 */
function video_shoot_signup_schedule_truncate_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] == 1 && !empty($form_state['values']['truncate']) && $form_state['values']['truncate'] == 'truncate') {
    db_truncate('video_signup_schedule')->execute();
    watchdog('video_shoot_signup', 'Video shoot schedule table truncated by request.', array(), WATCHDOG_WARNING);
  }
  drupal_goto('admin/config/hfc/video-shoot');
}
