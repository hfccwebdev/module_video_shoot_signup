<?php

/**
 * @file
 * Interface and Controller for Marketing Video Shoot Sign-ups.
 */

/**
 * VideoSchedule class.
 */
class VideoSchedule extends Entity {
  protected function defaultLabel() {
    $program = !empty($this->program_name) ? $this->program_name : 'Available timeslot';
    return t('@program on @datestamp', array('@program' => $program, '@datestamp' => format_date($this->start_date)));
  }
  protected function defaultUri() {
    return array('path' => 'faculty-and-staff/video-shoot/schedule/' . $this->identifier());
  }
}

/**
 * VideoScheduleController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class VideoScheduleController extends EntityAPIController {

  /**
   * Define display properties for entity contents.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $shoot = entity_load('video_signup_shoot', array($entity->shoot_id));
    $shoot = !empty($shoot) ? reset($shoot) : StdObject();

    $shoot_types = video_shoot_signup_types();

    $content['start_date'] = array(
      '#field_name' => 'video_signup_schedule_start_date',
      '#weight' => -9,
      '#label' => t('Start Time'),
      '#label_display' => 'inline',
      '#theme' => 'hfcc_global_pseudo_field',
      '#markup' => format_date($entity->start_date, 'medium'),
    );
    $content['end_date'] = array(
      '#field_name' => 'video_signup_schedule_end_date',
      '#weight' => -8,
      '#label' => t('End Time'),
      '#label_display' => 'inline',
      '#theme' => 'hfcc_global_pseudo_field',
      '#markup' => format_date($entity->end_date, 'medium'),
    );
    if (!empty($shoot->title)) {
      $content['shoot_name'] = array(
        '#field_name' => 'video_signup_schedule_shoot_name',
        '#weight' => -7,
        '#label' => t('Shoot'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => check_plain($shoot->title),
      );
    }
    if (!empty($shoot->type)) {
      $content['shoot_type'] = array(
        '#field_name' => 'video_signup_schedule_shoot_type',
        '#weight' => -6,
        '#label' => t('Shoot Type'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => !empty($shoot_types[$shoot->type]) ? t($shoot_types[$shoot->type]) : check_plain($shoot->type),
      );
    }
    if (!empty($entity->program_name)) {
      $content['program_name'] = array(
        '#field_name' => 'video_signup_schedule_program_name',
        '#weight' => -5,
        '#label' => t('Program'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => check_plain($entity->program_name),
      );
    }
    if (!empty($entity->contact_name)) {
      $content['contact_name'] = array(
        '#field_name' => 'video_signup_schedule_contact_name',
        '#weight' => -4,
        '#label' => t('Faculty Contact'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => video_shoot_signup_get_contact($entity->contact_name, 'full'),
      );
    }
    if (!empty($entity->location)) {
      $content['location'] = array(
        '#field_name' => 'video_signup_schedule_location',
        '#weight' => -3,
        '#label' => t('Class/Lab Location for Shoot'),
        '#label_display' => 'inline',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => check_plain($entity->location),
      );
    }
    if (!empty($entity->notes)) {
      $content['notes'] = array(
        '#field_name' => 'video_signup_schedule_notes',
        '#weight' => -2,
        '#label' => t('Notes'),
        '#label_display' => 'above',
        '#theme' => 'hfcc_global_pseudo_field',
        '#markup' => check_plain($entity->notes),
      );
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
